from fabric import Connection
from fabric.runners import Result
from socket import timeout
from time import sleep
from gi.repository import GObject

class SystemSignaler(GObject.Object):
    __gsignals__ = {
        'unifydmin_system_disconnected': (
            GObject.SIGNAL_RUN_FIRST,
            None,
            (str,)
        ),
        'unifydmin_system_reconnected': (
            GObject.SIGNAL_RUN_FIRST,
            None,
            (str,)
        )
    }

class System:
    
    def __init__(self, name: str, address: str, username: str):
        if not (name and address and username):
            raise ValueError('name, address and username must not be empty')
            return
        self.name = name
        self.address = address
        self.username = username
        self.online = False
        self.watchers = []
        # the connection to the server
        # can run commands to it with .run(cmd:str, **hide:bool, **warn:bool)
        self.connection = Connection(
            host = self.address,
            user = self.username
        )
        self.signaler = SystemSignaler()
        self.connect = self.signaler.connect
        self.emit = self.signaler.emit
        # establishes the connection, can throw a socket.timeout if connection fails
        #self.connection.open()

    def __disconnected(self):
        self.online = False
        print(f'{self} emitted disconnected')
        self.emit('unifydmin_system_disconnected', '')

    def __reconnected(self):
        if not self.online:
            self.online = True
            self.emit('unifydmin_system_reconnected', '')
    
    def run_as_root(self, cmd) -> Result:
        out = None
        try:
            root_connection = Connection(f'root@{self.address}', connect_timeout=15)
            root_connection.open()
            out = root_connection.run(cmd, hide=True, warn=True)
            root_connection.close()
            self.__reconnected()
            return out
        except Exception as e:
            print(f'System {self.__repr__()}: Error running root command `{cmd}`')
            print('   ', e)
            self.__disconnected()
            return None

    def run(self, cmd: str) -> Result:
        out = None
        try:
            out = self.connection.run(cmd, hide=True, warn=True)
            self.__reconnected()
            return out
        except timeout:
            print(f'System {self.__repr__()}: Timeout running command `{cmd}`')
            self.__disconnected()
            return None

    def run_action(self, action):
        if isinstance(action, action.WatcherAction):
            raise TypeError('The Action passed should NOT be a WatcherAction')
        else:
            action.run(self)

    def add_watcher(self, watcher):
        self.watchers.append(watcher(self))

    def to_dict(self):
        return {
            'name': self.name,
            'username': self.username,
            'address': self.address
        }
        
    def __repr__(self):
        return f'{self.name} - {self.username}@{self.address}'
    
    def __del__(self):
        self.connection.close()
